from mcct import MinecraftClient
import asyncio
from cubelib.proto import v47

async def en():
    client = MinecraftClient(v47)

    print("connecting")
    await client.connect("127.0.0.1", 25565)
    await client.start_event_dispatch()

    print("login")
    await client.login("seeklay")

    while 7:
        await asyncio.sleep(0.1)

if __name__ == "__main__":
    try:
        asyncio.run(en())
    except KeyboardInterrupt:
        ...
