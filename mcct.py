import asyncio
import cubelib
import time

class MinecraftClient:
    
    writer: asyncio.StreamWriter
    reader: asyncio.StreamReader

    protocol: cubelib.proto
    state: cubelib.state = cubelib.state.Handshaking
    compression: int = -1

    def __init__(self, protocol):
        self.protocol = protocol

    async def receiver(self) -> None:

        buffer: bytes = bytes()
        lt, rx = time.time(), 0

        while 7:
            r = await self.reader.read(65535)
            if not r:
                break

            buffer += r
            ps = []
            buffer = cubelib.rrPacketsStream(buffer, self.compression, cubelib.bound.Client, self.state, self.protocol, ps, 1 if self.state is not cubelib.state.Play else -1)
            for p in ps:
                if hasattr(self, f"_so_{p.__class__.__name__}"):
                    await (getattr(self, f"_so_{p.__class__.__name__}"))(p)
                #else:
                #    print("Unhandled:", p)

            rx += len(ps)
            if time.time() - lt >= 1:
                print("PPS: ", rx)
                lt, rx = time.time(), 0


    async def _so_SetCompression(self, packet):
        self.compression = packet.Threshold
    
    async def _so_LoginSuccess(self, _):
        self.state = cubelib.state.Play

    async def connect(self, host, port):
        """
            Connect to the server
        """

        self.host, self.port = host, port
        self.reader, self.writer = await asyncio.wait_for(asyncio.open_connection(host, port), timeout = 3)
    
    async def start_event_dispatch(self):
        asyncio.create_task(self.receiver())
    
    async def login(self, username: str) -> None:
        """
            Login to the server
        """

        await self.handshake(cubelib.state.Login)
        await self.sendp(self.protocol.ServerBound.Login.LoginStart(username))

    async def handshake(self, next_state: cubelib.state) -> None:
        """
            Handshake with the server
        """

        await self.sendp(cubelib.proto.ServerBound.Handshaking.Handshake(self.protocol.version, self.host, self.port, next_state))
        self.state = next_state
    
    async def sendp(self, packet: cubelib.p.Night) -> None:
        """
            Send packet to the server
        """

        self.writer.write(packet.build(self.compression))
        await self.writer.drain()
